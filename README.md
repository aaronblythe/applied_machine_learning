
The following homeworks are from University of Illinois [CS 498 Applied Machine Learning](https://courses.engr.illinois.edu/cs498aml/sp2019/) (Archived in this [Spring_2019_CS498AML.html](repository))

I used Jupyter for all of the assignments. Most were done in Python. One assignment (HW 6) was done in R. Most development was done in Google Colab to make use of their processing power. Formatting was done locally in Jupyter to utilize the formatting with LaTex to create a pdf.

I learned R much better in STAT 420 the semester following this semester of Apllied Machine Learning.

All of these assignments received a 100/100, except HW 5 which received 99/100 for having a gradient color rather than numbers displayed to represent a 14x14 Confusion Matrix. 

NOTE: each project had a requested format that had to be followed (for ease of grading by the TA's) This should explain the page breaks in the Jupyter files. 

# HW 1 - Classification with Naive Bayes

[https://colab.research.google.com/drive/1doMIdATU4Lgxwiq664em1PwVTmTcC9aX](Google Colab Link)

# HW 2 - Classification with Support Vector Machines (SVM)

[https://colab.research.google.com/drive/1EUzI1A8nSG7WqfZ4apnCzcax2tZahBAy](Google Colab Link)

NOTE: the turned in copy would have the TODO's with the screenshot.

# HW 3 - Smoothing Noise with Principal Component Analysis (PCA)

No Colab link

Autograded python file

# HW 4 - More Principal Component Analysis (PCA)

[https://colab.research.google.com/drive/13NjeraDoDESv_lXErcTi71n1XdQ2jmVi](Google Colab Link)

# HW 5 - Vector quantization - Classifying with Variable Length Inputs

[https://colab.research.google.com/drive/1KmzwGVGUuXc7P7AwuwtBz_3BoAXbXxhX](Google Colab Link)

# HW 6 - Outlier Detection

[https://colab.research.google.com/drive/1Ef5rTRk_6apFYwukROPdF6KRlWYPSOl9](Google Colab Link)

# HW 7 - Text bag-of-words Search and Classification

[https://colab.research.google.com/drive/1zjqW7svg49WzV_5lMvSoScIE0YBpF8pW](Google Colab Link)

# HW 8 - Classification with Artificial Neural Networks with PyTorch

[https://colab.research.google.com/drive/14sMjTCCK-nXI3y5-l0OLBdbxCAU3-3Kx](Google Colab Link)

# HW 9 - Variational Autoencoders

[https://colab.research.google.com/drive/1Qr-7gTBwbMTbajBpHusv2uHcTEM1XCLR](Google Colab Link)

# HW 10 - Deep Neural Networks - Convolutional Neural Networks with PyTorch

[https://colab.research.google.com/drive/12iauR6WPjcEejtAY-qS_s3DTVVUCN1LX](Google Colab Link)
