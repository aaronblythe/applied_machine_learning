From: https://courses.engr.illinois.edu/cs498aml/sp2019/homeworks/homework10.html


Homework 10: Convolutional Neural Networks with PyTorch

About
Due

Wednesday 5/1/19, 11:59 PM CST
Goal

In this homework set, you will extend from neural network with simple fully connected layers (as in Homework 8) and learn to use convolution layers, batch normailzation, and advanced features to improve the accuracy.  
Code and External Libraries

The assignment must be done using PyTorch platform and GPU. Do all of your work in the provided iPython notebook.

The libraries you may need to have are in this requirements.txt file.
Problems
Total points: 100

    Download the Python Notebook here. Alternatively, you can access a read-only version on colab here (update 4/19: added definition of Flatten class and train function in part 0) of which you will need to make a copy.
    There are cells for you to input code, as well as text. Make sure to fill in all such cells before submission. Important information and sections are in bold.
    The report requirement is described in the notebook also. Please check the detail there.

Submission

Submission will be through gradescope
Deliverables

    Your python notebook renamed as netid_HW10.ipynb. Submit this in the HW10 Code section.
    Convert your python notebook with all outputs and questions answered into PDF format. Name it netid_HW10.pdf. Submit this in the HW10 Report section

Note: Make sure that your training plots are visible after converting to PDF. If this becomes difficult, save the plots as images and attach them to the end of your PDF submission.
